module gitlab.cern.ch/bnl-omega-go/lpgbttool

go 1.17

require (
	github.com/codehardt/mmap-go v1.0.1 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220219041304-c5147b6a5b1a // indirect
	gitlab.cern.ch/bnl-omega-go/slowcontrol v0.0.0-20220302185740-b026fea0328b
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	periph.io/x/conn/v3 v3.6.10 // indirect
	periph.io/x/host/v3 v3.7.2 // indirect
)
