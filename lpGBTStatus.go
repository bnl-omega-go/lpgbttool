//go:build linux || darwin
// +build linux darwin

package main

import (
	"flag"
	"log"
	"math/rand"
	"time"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

func main() {

	i2cbus := flag.String("b", "/dev/i2c-2", "bus to be used for I2C communication ex: /dev/i2c-0,AXI etc.")
	devaddr := flag.Uint64("a", 0x68, "I2C address of the device")
	dryrun := flag.Bool("dry", false, "Dry run reading CSV file")
	v0 := flag.Bool("v0", false, "lpGBTx v0 (default false i.e. v1)")
	flag.Parse()

	ctrl := SlowControl.I2CController{}
	if !*dryrun {
		ctrl.Init()
		ctrl.AddDevice("lpGBT", uint16(*devaddr), *i2cbus)
	}

	address := 0x1d9 // status register v1
	rom_test_reg_addr := 0x1d7
	if *v0 {
		address = 0x1c7 // status register v0
		rom_test_reg_addr = 0x1c5
	}

	reg_add_l := address & 0xFF
	reg_add_h := (address >> 8) & 0xFF
	payload := []byte{byte(reg_add_l), byte(reg_add_h)}

	rand.Seed(time.Now().UTC().UnixNano())
	status := []byte{byte(rand.Intn(19))}
	var err error
	if !*dryrun {
		ctrl.Write("lpGBT", payload)
		status, err = ctrl.Read("lpGBT", 1)

		if err != nil {
			log.Fatal(err)
		}
	}

	log.Println("lpGBTx Power-up FSM's status is:")
	switch status[0] & 0x1F {
	case 0:
		log.Println("ARESET")
	case 1:
		log.Println("RESET1")
	case 2:
		log.Println("WAIT_VDD_STABLE")
	case 3:
		log.Println("WAIT_VDD_HIGHER_THAN_0V90")
	case 4:
		log.Println("COPY_FUSES")
	case 5:
		log.Println("CALCULATE_CHECKSUM")
	case 6:
		log.Println("COPY_ROM")
	case 7:
		log.Println("PAUSE_FOR_PLL_CONFIG_DONE")
	case 8:
		log.Println("WAIT_POWER_GOOD")
	case 9:
		log.Println("RESET_PLL")
	case 10:
		log.Println("WAIT_PLL_LOCK")
	case 11:
		log.Println("INIT_SCRAM")
	case 12:
		log.Println("RESETOUT")
	case 13:
		log.Println("I2C_TRANS")
	case 14:
		log.Println("PAUSE_FOR_DLL_CONFIG_DONE")
	case 15:
		log.Println("RESET_DLLS")
	case 16:
		log.Println("WAIT_DLL_LOCK")
	case 17:
		log.Println("RESET_LOGIC_USING_DLL")
	case 18:
		log.Println("WAIT_CHNS_LOCKED")
	case 19:
		log.Println("READY")
	default:
		log.Println("UNKNOWN")
	}

	// TEST ROM REG
	status = []byte{byte(rand.Intn(19))}

	rom_test_reg_add_l := rom_test_reg_addr & 0xFF
	rom_test_reg_add_h := (rom_test_reg_addr >> 8) & 0xFF
	rom_test_payload := []byte{byte(rom_test_reg_add_l), byte(rom_test_reg_add_h)}

	if !*dryrun {
		ctrl.Write("lpGBT", rom_test_payload)
		status, err = ctrl.Read("lpGBT", 1)
		if err != nil {
			log.Fatal(err)
		}
	}

	switch status[0] {
	case 0xA5:
		log.Printf("Correct lpGBT ROM test register (v0) : %v", status[0])
	case 0xA6:
		log.Printf("Correct pGBT ROM test register (v1) : %v", status[0])
	default:
		log.Printf("Wrong lpGBT ROM test register (v?) : %v", status[0])
	}

}
