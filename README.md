# lpGBT Tool

Small go tool to upload lpGBT config file from piGBT into the lpGBT via I2C system bus. 

This tool allows to read a Configuration file generated using piGBT and upload it to the lpGBT via I2C bus, and to verify the PUSM Status when providing clock. 

The go tools must be installed on the machine where the compilation will take place. 

## Compilation

To compile : 

```
go mod tidy
go build lpGBTTool.go 
go build lpGBTStatus.go
```

To compile for a different platform, most likely arm64/linux : 

```
go mod tidy 
GOOS=linux GOARCH=arm64 go build lpGBTTool.go
GOOS=linux GOARCH=arm64 go build lpGBTStatus.go
```

The produced executable can be transfered to your arm platform for usage. 

## Usage 

You can use the -h option of the executable to see the available parameters : 

```
../lpGBTTool -h
Usage of ./lpGBTTool:
  -a uint
        I2C address of the device (default 104)
  -b string
        bus to be used for I2C communication ex: /dev/i2c-0,AXI etc. (default "/dev/i2c-2")
  -dry
        Dry run reading CSV file
  -f string
        Conf file containing configuration for the lpGBT (default "lpGBT.conf")
```

The configuration file can contains comments (starting with the # charater). 

The status tool simply print the lpGBT Status when provided with bus and address : 

```
Usage of ./lpGBTStatus:
  -a uint
        I2C address of the device (default 104)
  -b string
        bus to be used for I2C communication ex: /dev/i2c-0,AXI etc. (default "/dev/i2c-2")
```

## Precompiled binaries 

You can always download the precompiled binaries from the artifacts produced during CI  : 

* [lpGBTTool amd64](https://gitlab.cern.ch/bnl-omega-go/lpgbttool/-/jobs/artifacts/master/raw/lpGBTTool?job=build-job)
* [lpGBTTool arm32](https://gitlab.cern.ch/bnl-omega-go/lpgbttool/-/jobs/artifacts/master/raw/lpGBTTool_arm32?job=build_arm32-job)
* [lpGBTTool arm64](https://gitlab.cern.ch/bnl-omega-go/lpgbttool/-/jobs/artifacts/master/raw/lpGBTTool_arm64?job=build_arm64-job)

* [lpGBTStatus amd64](https://gitlab.cern.ch/bnl-omega-go/lpgbttool/-/jobs/artifacts/master/raw/lpGBTStatus?job=build-job)
* [lpGBTStatus arm32](https://gitlab.cern.ch/bnl-omega-go/lpgbttool/-/jobs/artifacts/master/raw/lpGBTStatus_arm32?job=build_arm32-job)
* [lpGBTStatus arm64](https://gitlab.cern.ch/bnl-omega-go/lpgbttool/-/jobs/artifacts/master/raw/lpGBTStatus_arm64?job=build_arm64-job)
